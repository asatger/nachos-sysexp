// exception.cc 
//      Entry point into the Nachos kernel from user programs.
//      There are two kinds of things that can cause control to
//      transfer back to here from user code:
//
//      syscall -- The user code explicitly requests to call a procedure
//      in the Nachos kernel.  Right now, the only function we support is
//      "Halt".
//
//      exceptions -- The user code does something that the CPU can't handle.
//      For instance, accessing memory that doesn't exist, arithmetic errors,
//      etc.  
//
//      Interrupts (which can also cause control to transfer from user
//      code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#ifdef CHANGED
#include "userthread.h"
#endif

//----------------------------------------------------------------------
// UpdatePC : Increments the Program Counter register in order to resume
// the user program immediately after the "syscall" instruction.
//----------------------------------------------------------------------
static void
UpdatePC ()
{
    int pc = machine->ReadRegister (PCReg);
    machine->WriteRegister (PrevPCReg, pc);
    pc = machine->ReadRegister (NextPCReg);
    machine->WriteRegister (PCReg, pc);
    pc += 4;
    machine->WriteRegister (NextPCReg, pc);
}


//----------------------------------------------------------------------
// ExceptionHandler
//      Entry point into the Nachos kernel.  Called when a user program
//      is executing, and either does a syscall, or generates an addressing
//      or arithmetic exception.
//
//      For system calls, the following is the calling convention:
//
//      system call code -- r2
//              arg1 -- r4
//              arg2 -- r5
//              arg3 -- r6
//              arg4 -- r7
//
//      The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//      "which" is the kind of exception.  The list of possible exceptions 
//      are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler (ExceptionType which)
{
    int type = machine->ReadRegister (2);

    switch (which)
      {
	case SyscallException:
          {
	    switch (type)
	      {
		case SC_Halt:
		  {
		    DEBUG ('s', "Shutdown, initiated by user program.\n");
		    interrupt->Halt ();
		    break;
		  }
		#ifdef CHANGED
		case SC_PutChar:
		  {
		  	write_mutex->P();
		  	
		    DEBUG ('s', "PutChar\n");
		    synchconsole->SynchPutChar (machine->ReadRegister (4));	
		    	    
		  	write_mutex->V();
		    break;
		  }
		  #endif //CHANGED
		#ifdef CHANGED
		case SC_PutString:
		  {
		  	write_mutex->P();
		  	
		    DEBUG ('s', "PutString\n");
		    
		    char to[MAX_STRING_SIZE];
		    int from = machine->ReadRegister(4);
		    while(synchconsole->copyStringFromMachine (from, to, MAX_STRING_SIZE)) {
		    	synchconsole->SynchPutString(to);
		    	from = from + strlen (to);
		    }
		    
		  	write_mutex->V();
			
		    break;
		  }
		  #endif //CHANGED
		#ifdef CHANGED
		case SC_GetString:
		  {
		  	read_mutex->P();
		  	
		    DEBUG ('s', "GetString\n");
		    
		    int to = machine->ReadRegister(4);
		    int size = machine->ReadRegister(5);
		    char from[size];
		    synchconsole->SynchGetString(from, size);
			size = synchconsole->copyStringToMachine (to, from, size);
			
		  	read_mutex->V();	    
		    
		    break;
		  }
		#endif //CHANGED
		#ifdef CHANGED
		case SC_PutInt:
			{
				write_mutex->P();
				
				int n;
				n = machine->ReadRegister(4);
				synchconsole->SynchPutInt(n);
				
				write_mutex->V();
				break;
			}
		#endif //CHANGED
		#ifdef CHANGED
		case SC_GetInt:
			{
				read_mutex->P();

				int n;
				int to;
				to = machine->ReadRegister(4);
				synchconsole->SynchGetInt(&n);
				machine->WriteMem(to, sizeof(int), n);

				read_mutex->V();
				break;
			}
		#endif //CHANGED
		#ifdef CHANGED		
        case SC_ThreadCreate:
        {
		  	read_mutex->P();
		  	
            int f = machine->ReadRegister(4);
            int arg = machine->ReadRegister(5);
            do_ThreadCreate(f, arg);
            
		  	read_mutex->V();
            break;
        }
		#endif
		#ifdef CHANGED		
        case SC_ForkExec:
        {	
		  	write_mutex->P();  
        	int _FILENAME_SIZE_MAX = 64;
            char filename[_FILENAME_SIZE_MAX];
            int filename_addr = machine->ReadRegister(4);
            
            synchconsole->copyStringFromMachine(filename_addr, filename, _FILENAME_SIZE_MAX);
            
            do_ForkExec(filename);
            
		  	write_mutex->V();    

            break;
        }
		#endif
        #ifdef SC_ThreadExit
            case SC_ThreadExit:
            {
				do_ThreadExit();
                break;
            }
        #endif
		#ifdef CHANGED
		case SC_Exit:
		  {
			int err = machine->ReadRegister (4);
			if(err == 1) {
				printf("Error --> %d\n", err);
			}
			interrupt-> Halt();
		    break;
		  }
		  #endif //CHANGED
		default:
		  {
		    printf("Unimplemented system call %d\n", type);
		    ASSERT(FALSE);
		  }
	      }

	    // Do not forget to increment the pc before returning!
	    UpdatePC ();
	    break;
	  }

	case PageFaultException:
	  if (!type) {
	    printf("NULL dereference at PC %x!\n", machine->registers[PCReg]);
	    ASSERT (FALSE);
	  } else {
	    printf ("Page Fault at address %x at PC %x\n", type, machine->registers[PCReg]);
	    ASSERT (FALSE);	// For now
	  }
	  break;

	default:
	  printf ("Unexpected user mode exception %d %d at PC %x\n", which, type, machine->registers[PCReg]);
	  ASSERT (FALSE);
      }
}
