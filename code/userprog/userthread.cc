#ifdef CHANGED
#include "copyright.h"
#include "system.h"
#include "userthread.h"
#include "machine.h"
#include "syscall.h"
#include "synch.h"
#include "new"

struct Schmurtz {
    int f;
    int arg;
};

void StartUserThread(void* schmurtz) {
    struct Schmurtz *cpySchmurtz;
    cpySchmurtz=(struct Schmurtz *)schmurtz;

    int i;
    for (i = 0; i < NumTotalRegs; i++) {
	    machine->WriteRegister (i, 0);
    }

    // Initial program counter -- must be location of "Start"

    machine->WriteRegister (PCReg, cpySchmurtz->f);

    DEBUG ('a', "Mon debug 0x%x\n", PCReg);

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister (NextPCReg, cpySchmurtz->f+4);

    DEBUG ('a', "Mon debug 0x%x\n", NextPCReg);

    machine->WriteRegister (4, cpySchmurtz->arg);

    // Set the stack register to the end of the address space, where we
    // allocated the stack; but subtract off a bit, to make sure we don't
    // accidentally reference off the end!
    
    int userStackAddr = currentThread->space->AllocateUserStack();
    
    if(userStackAddr == -1) {
    	printf("ERROR USERSTACK \n");
    }
    
    machine->WriteRegister (StackReg, userStackAddr);

    DEBUG ('a', "Initializing stack register to 0x%x\n", userStackAddr);

    machine-> Run();
}

int do_ThreadCreate(int f, int arg) {

    struct Schmurtz* schmurtz = new struct Schmurtz;

    schmurtz->f = f;
    schmurtz->arg = arg;

    currentThread->space->user_thread_counter_semaphore->P();
    currentThread->space->countThreads ++;
    currentThread->space->user_thread_counter_semaphore->V();

    Thread *newThread = new Thread("newThread#"+currentThread->space->countThreads);

    newThread-> Start(StartUserThread, (void*)schmurtz);    

    return 0;
    
}

void do_ThreadExit() {
    currentThread->space->user_thread_counter_semaphore->P();
    
    currentThread->space->countThreads--;
    
    currentThread->space->user_thread_counter_semaphore->V();
    
    if(currentThread->space->countThreads > 0) {
    	int stackRegister = machine->ReadRegister(StackReg);
    	currentThread->space->ClearUserStack(stackRegister);
        currentThread-> Finish(); 
    } else {
        currentThread->space->user_thread_counter_semaphore->P();            
        nbProcess--;            
        currentThread->space->user_thread_counter_semaphore->V();
        if(nbProcess > 0) {
            delete currentThread->space;
            currentThread-> Finish();
        } else {
            interrupt-> Halt();
        }       
    }
}

void StartForkThread(void *arg) {

    currentThread->space->InitRegisters();

    currentThread->space->RestoreState(); 
    machine->DumpMem("memory.svg");

    machine->Run();
    ASSERT(FALSE);
}

int do_ForkExec(char filename[]) {
	AddrSpace *addrspace;
	Thread *fork;
	OpenFile *file;            

	file = fileSystem->Open(filename);

	if(file == NULL) {
		printf("Unable to open file %s\n", filename);	
		return -1;
	}
		
    try { 
    	addrspace = new AddrSpace(file);
    }catch(const std::bad_alloc& e) {
    	printf("==> BAD ALLOC ERROR: no empty space\n");
    	delete addrspace;
    	ASSERT(false);
    }
    
	fork = new Thread("forkThread");

	currentThread->space->user_thread_counter_semaphore->P();
    nbProcess++;
	currentThread->space->user_thread_counter_semaphore->V();
	
	fork->space = addrspace;

	fork->Start(StartForkThread, NULL);

	delete file;
	
	return 0;
}

#endif
