#ifdef CHANGED
#ifndef USERTHREAD_H
#define USERTHREAD_H

#include "copyright.h"
#include "utility.h"
#include "list.h"

#ifdef USER_PROGRAM
#include "machine.h"
#include "addrspace.h"
#endif

extern int do_ThreadCreate(int f, int arg);
extern void do_ThreadExit();
extern int do_ForkExec(char filename[]);

class Userthread {
    private:

    public: 

    static void StartUserThread(void*schmurtz);
    static void StartForkThread(void*arg);
};

#endif
#endif
