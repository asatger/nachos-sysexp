
#ifndef PAGEPROVIDER_H
#define PAGEPROVIDER_H
#ifdef CHANGED

#include "bitmap.h"

class PageProvider {
public:
	PageProvider(int n);
	~PageProvider();
	int GetEmptyPage();
	void ReleasePage(int n);
	int NumAvailPage();
	BitMap* page;
};

#endif
#endif // PAGEPROVIDER_H