#ifdef CHANGED
#include "copyright.h"
#include "system.h"
#include "synchconsole.h"
#include "synch.h"
static Semaphore *readAvail;
static Semaphore *writeDone;
static Semaphore *semaphore;
static void ReadAvailHandler(void *arg)
{
    (void)arg;
    readAvail->V();
}
static void WriteDoneHandler(void *arg)
{
    (void)arg;
    writeDone->V();
}

SynchConsole::SynchConsole(const char *in, const char *out)
{
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 0);
    semaphore = new Semaphore("semaphore", 1);
    console = new Console(in, out, ReadAvailHandler, WriteDoneHandler, 0);
}

SynchConsole::~SynchConsole()
{
    delete console;
    delete writeDone;
    delete readAvail;
    delete semaphore;
}

void SynchConsole::SynchPutChar(int ch)
{
    semaphore->P();
    console->PutChar(ch);
    writeDone->P();
    semaphore->V();

}

int SynchConsole::SynchGetChar()
{

    semaphore->P();
    readAvail->P();
    int c = console->GetChar();
    semaphore->V();
    return c;
}

void SynchConsole::SynchPutString(const char s[])
{
    int i = 0;
    while(s[i] != '\0' && i < MAX_STRING_SIZE) {
        SynchPutChar(s[i]);
        i++;
    }
}

void SynchConsole::SynchGetString(char *s, int n)
{
    int i = 0;
    char c = SynchGetChar();
    while( i < n && c!='\n' && c!=EOF) {
       	s[i]=c;
        i++;
        c = SynchGetChar();
    }
    s[i]= '\0';
}

#ifdef CHANGED
void SynchConsole::SynchPutInt(int n) {
	char s[MAX_STRING_SIZE];
	snprintf(s, MAX_STRING_SIZE, "%i", n);
	SynchPutString(s);
}

void SynchConsole::SynchGetInt(int *n) {
	char s[MAX_STRING_SIZE];
	SynchGetString(s, MAX_STRING_SIZE);
	sscanf(s, "%i", n);
}
#endif


int SynchConsole::copyStringFromMachine(int from, char *to, unsigned size) {
    int buffer;
    unsigned int i;
  
    for(i = 0; i < size; i++) {
        if((char)buffer == '\0' || (char)buffer == '\n') {
            break;
        }
        machine->ReadMem(from + i, 1, &buffer);
        to[i] = (char) buffer;
    }

    to[i]='\0';

    return strlen(to);
}

int SynchConsole::copyStringToMachine(int to, char *from, unsigned size) {
    unsigned int i;
  
    for(i = 0; i < size; i++) {
        if((char)from[i] == '\0' || (char)from[i] == '\n') {
            break;
        }
        machine->WriteMem(to + i, 1, (int)from[i]);
    }
    machine->WriteMem(to + i, 1, '\0');
    
    return i;
}


#endif // CHANGED
