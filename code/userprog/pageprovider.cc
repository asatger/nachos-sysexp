#ifdef CHANGED
#include "bitmap.h"
#include "pageprovider.h"
#include "system.h"

PageProvider::PageProvider(int n) {
	page = new BitMap(n);
    page->Mark(0);
}

PageProvider::~PageProvider(){
	delete page;
}

int PageProvider::GetEmptyPage() {
	int emptyPage = page->Find();
    ASSERT(emptyPage != -1);

    memset(machine->mainMemory+emptyPage * PageSize, 0, PageSize); 
    return emptyPage;
}

void PageProvider::ReleasePage(int n) {
    page->Clear(n);
}

int PageProvider::NumAvailPage() {
    return page->NumClear();
}


#endif //CHANGED
