#include "syscall.h"

main() {

    int i;
    for(i = 0; i < 12; i++) {
        if(i%2 == 0) {
        	PutChar('\n');
        	PutString("==> Fork user 0 #");
        	PutInt(i);
        	PutChar('\n');
            ForkExec("test/userpages0");
        } else {
        	PutChar('\n');
        	PutString("==> Fork user 1 #");
        	PutInt(i);
        	PutChar('\n');
            ForkExec("test/userpages1");
        }
    }

    ThreadExit();
    
    return 0;
}
