#include "syscall.h"


void putchar(void *arg) {
	volatile int i;
    for(i = 0; i < 3; ++i) {
    	PutChar(*((char*)arg));
    }
    //PutChar('\n');
    ThreadExit();
}


int main() {
	int i;
	char c1 = 'A';
	char c2 = 'a';
	
	ThreadCreate(putchar, (void*) &c1);
	ThreadCreate(putchar, (void*) &c2);
	ThreadCreate(putchar, (void*) &c1);
	ThreadCreate(putchar, (void*) &c2);
	ThreadCreate(putchar, (void*) &c2);
	ThreadCreate(putchar, (void*) &c1);
	
	ThreadExit();
	return 0;
}
